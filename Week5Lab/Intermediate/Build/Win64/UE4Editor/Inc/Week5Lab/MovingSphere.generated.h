// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEEK5LAB_MovingSphere_generated_h
#error "MovingSphere.generated.h already included, missing '#pragma once' in MovingSphere.h"
#endif
#define WEEK5LAB_MovingSphere_generated_h

#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_SPARSE_DATA
#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_RPC_WRAPPERS
#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMovingSphere(); \
	friend struct Z_Construct_UClass_AMovingSphere_Statics; \
public: \
	DECLARE_CLASS(AMovingSphere, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Week5Lab"), NO_API) \
	DECLARE_SERIALIZER(AMovingSphere)


#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMovingSphere(); \
	friend struct Z_Construct_UClass_AMovingSphere_Statics; \
public: \
	DECLARE_CLASS(AMovingSphere, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Week5Lab"), NO_API) \
	DECLARE_SERIALIZER(AMovingSphere)


#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMovingSphere(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMovingSphere) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingSphere); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingSphere); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingSphere(AMovingSphere&&); \
	NO_API AMovingSphere(const AMovingSphere&); \
public:


#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingSphere(AMovingSphere&&); \
	NO_API AMovingSphere(const AMovingSphere&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingSphere); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingSphere); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMovingSphere)


#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_PRIVATE_PROPERTY_OFFSET
#define Week5Lab_Source_Week5Lab_MovingSphere_h_9_PROLOG
#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_PRIVATE_PROPERTY_OFFSET \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_SPARSE_DATA \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_RPC_WRAPPERS \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_INCLASS \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Week5Lab_Source_Week5Lab_MovingSphere_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_PRIVATE_PROPERTY_OFFSET \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_SPARSE_DATA \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_INCLASS_NO_PURE_DECLS \
	Week5Lab_Source_Week5Lab_MovingSphere_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEEK5LAB_API UClass* StaticClass<class AMovingSphere>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Week5Lab_Source_Week5Lab_MovingSphere_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
