// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Week5Lab/MovingSphere.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovingSphere() {}
// Cross Module References
	WEEK5LAB_API UClass* Z_Construct_UClass_AMovingSphere_NoRegister();
	WEEK5LAB_API UClass* Z_Construct_UClass_AMovingSphere();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Week5Lab();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void AMovingSphere::StaticRegisterNativesAMovingSphere()
	{
	}
	UClass* Z_Construct_UClass_AMovingSphere_NoRegister()
	{
		return AMovingSphere::StaticClass();
	}
	struct Z_Construct_UClass_AMovingSphere_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Checkpoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Checkpoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Checkpoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMovingSphere_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Week5Lab,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMovingSphere_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MovingSphere.h" },
		{ "ModuleRelativePath", "MovingSphere.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints_Inner = { "Checkpoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints_MetaData[] = {
		{ "Category", "MovingSphere" },
		{ "ModuleRelativePath", "MovingSphere.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints = { "Checkpoints", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMovingSphere, Checkpoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMovingSphere_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMovingSphere_Statics::NewProp_Checkpoints,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMovingSphere_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMovingSphere>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMovingSphere_Statics::ClassParams = {
		&AMovingSphere::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMovingSphere_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMovingSphere_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMovingSphere_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMovingSphere_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMovingSphere()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMovingSphere_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMovingSphere, 2669703190);
	template<> WEEK5LAB_API UClass* StaticClass<AMovingSphere>()
	{
		return AMovingSphere::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMovingSphere(Z_Construct_UClass_AMovingSphere, &AMovingSphere::StaticClass, TEXT("/Script/Week5Lab"), TEXT("AMovingSphere"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMovingSphere);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
