// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Week5Lab/Week5LabGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeek5LabGameModeBase() {}
// Cross Module References
	WEEK5LAB_API UClass* Z_Construct_UClass_AWeek5LabGameModeBase_NoRegister();
	WEEK5LAB_API UClass* Z_Construct_UClass_AWeek5LabGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Week5Lab();
// End Cross Module References
	void AWeek5LabGameModeBase::StaticRegisterNativesAWeek5LabGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AWeek5LabGameModeBase_NoRegister()
	{
		return AWeek5LabGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AWeek5LabGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWeek5LabGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Week5Lab,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeek5LabGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Week5LabGameModeBase.h" },
		{ "ModuleRelativePath", "Week5LabGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWeek5LabGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWeek5LabGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWeek5LabGameModeBase_Statics::ClassParams = {
		&AWeek5LabGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AWeek5LabGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWeek5LabGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWeek5LabGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWeek5LabGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeek5LabGameModeBase, 528190503);
	template<> WEEK5LAB_API UClass* StaticClass<AWeek5LabGameModeBase>()
	{
		return AWeek5LabGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeek5LabGameModeBase(Z_Construct_UClass_AWeek5LabGameModeBase, &AWeek5LabGameModeBase::StaticClass, TEXT("/Script/Week5Lab"), TEXT("AWeek5LabGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeek5LabGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
