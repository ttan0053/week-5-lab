// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEEK5LAB_Week5LabGameModeBase_generated_h
#error "Week5LabGameModeBase.generated.h already included, missing '#pragma once' in Week5LabGameModeBase.h"
#endif
#define WEEK5LAB_Week5LabGameModeBase_generated_h

#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_SPARSE_DATA
#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_RPC_WRAPPERS
#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeek5LabGameModeBase(); \
	friend struct Z_Construct_UClass_AWeek5LabGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AWeek5LabGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Week5Lab"), NO_API) \
	DECLARE_SERIALIZER(AWeek5LabGameModeBase)


#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWeek5LabGameModeBase(); \
	friend struct Z_Construct_UClass_AWeek5LabGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AWeek5LabGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Week5Lab"), NO_API) \
	DECLARE_SERIALIZER(AWeek5LabGameModeBase)


#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeek5LabGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeek5LabGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeek5LabGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeek5LabGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeek5LabGameModeBase(AWeek5LabGameModeBase&&); \
	NO_API AWeek5LabGameModeBase(const AWeek5LabGameModeBase&); \
public:


#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeek5LabGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeek5LabGameModeBase(AWeek5LabGameModeBase&&); \
	NO_API AWeek5LabGameModeBase(const AWeek5LabGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeek5LabGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeek5LabGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeek5LabGameModeBase)


#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_12_PROLOG
#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_SPARSE_DATA \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_RPC_WRAPPERS \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_INCLASS \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_SPARSE_DATA \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEEK5LAB_API UClass* StaticClass<class AWeek5LabGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Week5Lab_Source_Week5Lab_Week5LabGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
