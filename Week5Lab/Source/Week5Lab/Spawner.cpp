// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerManager.h"
#include "Spawner.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StartText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("StartNumber"));
	StartText->SetHorizontalAlignment(EHTA_Center);
	StartText->SetWorldSize(150.0f);
	RootComponent = StartText;

	SpawnInterval = 3;
	StartTime = 3;
	SpawnCountdown = 0;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	UpdateTimerDisplay();
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawner::AdvancedTimer, 1.0f, true);
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (StartSpawning)
	{
		SpawnCountdown -= DeltaTime;
		if (SpawnCountdown <= 0)
		{
			AMovingSphere* tempReference = GetWorld()->SpawnActor<AMovingSphere>(SpawnObject, this->GetActorLocation(), FRotator::ZeroRotator);
			SpawnCountdown = SpawnInterval;
		}
	}

}

void ASpawner::UpdateTimerDisplay()
{
	StartText->SetText(FString::FromInt(FMath::Max(StartTime, 0)));
}

void ASpawner::AdvancedTimer()
{
	--StartTime;
	UpdateTimerDisplay();
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		CountdownHasFinished();
	}
}

void ASpawner::CountdownHasFinished()
{
	StartText->SetText(TEXT("Go!"));
	StartSpawning = true;
}
