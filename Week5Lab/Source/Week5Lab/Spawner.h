// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/TextRenderComponent.h"
#include "MovingSphere.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class WEEK5LAB_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float SpawnInterval;

	UPROPERTY(EditAnywhere)
		int32 StartTime;

	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject;

	bool StartSpawning = false;
	float SpawnCountdown;

	UTextRenderComponent* StartText;

	void UpdateTimerDisplay();

	void AdvancedTimer();

	void CountdownHasFinished();

	FTimerHandle StartTimerHandle;
};
