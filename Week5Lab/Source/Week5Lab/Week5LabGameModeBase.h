// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Week5LabGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class WEEK5LAB_API AWeek5LabGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
